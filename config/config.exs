use Mix.Config

config :ednsd,
  port: System.get_env("PORT") || 5053

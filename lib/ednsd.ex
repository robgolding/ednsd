defmodule Ednsd do
  use Application

  defp get_port() do
    parse_port(Application.get_env(:ednsd, :port))
  end

  defp parse_port(port) when is_integer(port), do: port

  defp parse_port(port) when is_binary(port), do: String.to_integer(port)

  def start(_type, _args) do
    children = [
      {Ednsd.Server, get_port()},
      {Ednsd.Cache, name: Ednsd.Cache},
    ]
    Supervisor.start_link(children, strategy: :one_for_one)
  end

  def resolve(record) do
    {:ok, socket} = :gen_udp.open(0, [:binary, active: false])
    :gen_udp.send(socket, {1, 1, 1, 1}, 53, DNS.Record.encode(record))
    {:ok, {ip, port, data}} = :gen_udp.recv(socket, 128)
    DNS.Record.decode(data)
  end
end

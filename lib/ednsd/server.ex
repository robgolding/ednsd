defmodule Ednsd.Server do
  require Logger
  use GenServer

  def start_link(port) do
    GenServer.start_link(__MODULE__, port)
  end

  def init(port) do
    Logger.info("Listening on port #{port}")
    {:ok, socket} = :gen_udp.open(port, [:binary, active: true])
    {:ok, %{socket: socket}}
  end

  def handle_info({:udp, socket, address, port, data}, state) do
    handle_packet({socket, address, port, data}, state)
  end

  defp handle_packet({socket, address, port, "quit\n"}, state) do
    :gen_udp.close(socket)
    {:stop, :normal, nil}
  end

  def do_resolve(record) do
    IO.inspect(record.qdlist)
    [ query | _ ] = record.qdlist
    response = Ednsd.resolve(record)
    Ednsd.Cache.populate(Ednsd.Cache, query.domain, response.anlist)
    response
  end

  defp resolve(record) do
    [ query | _ ] = record.qdlist
    resources = Ednsd.Cache.get(Ednsd.Cache, query.domain)
    case resources do
      [] -> do_resolve(record)
      resources -> Map.put(record, :anlist, resources)
    end
  end

  defp handle_packet({socket, address, port, data}, state) do
    response = data |> DNS.Record.decode() |> resolve() |> DNS.Record.encode()
    :gen_udp.send(socket, address, port, response)
    {:noreply, state}
  end
end

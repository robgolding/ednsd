defmodule Ednsd.Cache do
  require Logger
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    {:ok, %{}}
  end

  def populate(server, domain, resources) do
    GenServer.cast(server, {:populate, to_string(domain), resources})
  end

  def get(server, domain) do
    GenServer.call(server, {:get, to_string(domain)})
  end

  def now() do
    :os.system_time(:seconds)
  end

  def _get(domain, state) do
    {resources, added} = Map.get(state, domain, {[], 0})
    diff = now() - added
    resources
    |> Enum.map(fn r -> Map.put(r, :ttl, r.ttl - diff) end)
    |> Enum.filter(fn r -> r.ttl > 0 end)
  end

  def _populate(domain, resources, state) do
    existing_resources = _get(domain, state)
    Map.put(state, domain, {resources ++ existing_resources, now()})
  end

  def handle_cast({:populate, domain, resources}, state) do
    IO.puts("populating #{domain} with...")
    IO.inspect(resources)
    new = _populate(domain, resources, state)
    IO.inspect(new)
    {:noreply, new}
  end

  def handle_call({:get, domain}, _from, state) do
    resources = _get(domain, state)
    IO.puts("cache returns...")
    IO.inspect(resources)
    {:reply, resources, state}
  end
end
